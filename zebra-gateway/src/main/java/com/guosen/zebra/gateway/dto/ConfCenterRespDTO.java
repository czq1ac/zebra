package com.guosen.zebra.gateway.dto;

/**
 * 配置中心响应DTO
 */
public class ConfCenterRespDTO {
    /**
     * 结果码
     */
    private int code;

    /**
     * 结果消息
     */
    private String msg;

    /**
     * 数据
     */
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
