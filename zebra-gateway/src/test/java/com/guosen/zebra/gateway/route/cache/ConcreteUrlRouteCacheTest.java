package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Tested;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class ConcreteUrlRouteCacheTest {

    @Tested
    private ConcreteUrlRouteCache concreteUrlRouteCache;

    @Test
    public void testUpdate() {

        // 初始化
        String firstService = "firstService";

        RouteInfo firstServiceHelloRouteInfo = RouteInfo.newBuilder().build();
        RouteInfo firstServiceEchoRouteInfo = RouteInfo.newBuilder().build();

        Map<String, RouteInfo> firstServiceRouteInfo = new HashMap<>();
        firstServiceRouteInfo.put("/firstService/hello", firstServiceHelloRouteInfo);
        firstServiceRouteInfo.put("/firstService/echo", firstServiceEchoRouteInfo);

        concreteUrlRouteCache.update(firstService, firstServiceRouteInfo);

        String secondService = "secondService";

        RouteInfo secondServiceHelloRouteInfo = RouteInfo.newBuilder()
                .method("hello")
                .build();
        RouteInfo secondServiceHiRouteInfo = RouteInfo.newBuilder()
                .method("hi")
                .build();

        Map<String, RouteInfo> secondServiceRouteInfo = new HashMap<>();
        secondServiceRouteInfo.put("/secondService/hello", secondServiceHelloRouteInfo);
        secondServiceRouteInfo.put("/secondService/hi", secondServiceHiRouteInfo);

        concreteUrlRouteCache.update(secondService, secondServiceRouteInfo);

        // 然后更新 secondService
        RouteInfo secondServiceFooRouteInfo = RouteInfo.newBuilder()
                .method("foo")
                .build();
        RouteInfo secondServiceBarRouteInfo = RouteInfo.newBuilder()
                .method("bar")
                .build();

        Map<String, RouteInfo> newSecondServiceRouteInfo = new HashMap<>();
        newSecondServiceRouteInfo.put("/secondService/foo", secondServiceFooRouteInfo);
        newSecondServiceRouteInfo.put("/secondService/bar", secondServiceBarRouteInfo);

        concreteUrlRouteCache.update(secondService, newSecondServiceRouteInfo);

        // check
        Map<String, RouteInfo> routeInfoMap = concreteUrlRouteCache.getUrlRouteInfoMap();


        assertThat(routeInfoMap.get("/firstService/hello"), is(firstServiceHelloRouteInfo));
        assertThat(routeInfoMap.get("/firstService/echo"), is(firstServiceEchoRouteInfo));
        assertThat(routeInfoMap.get("/secondService/foo"), is(secondServiceFooRouteInfo));
        assertThat(routeInfoMap.get("/secondService/bar"), is(secondServiceBarRouteInfo));

        assertThat(routeInfoMap.get("/secondService/hello"), is(nullValue()));
        assertThat(routeInfoMap.get("/secondService/hi"), is(nullValue()));
    }
}
